#!/bin/bash
rm -rf nginx
rm -rf .env
cp -R nginx_prd nginx
cp setup.container .env

rm -rf /var/cedro/configs/appsettings.json
cp services/appsettings.prd.json services/Backlog.API/appsettings.json
cp services/appsettings.prd.json services/BackLogPDL.API/appsettings.json
cp services/appsettings.prd.json services/BackOffice.API/appsettings.json
cp services/appsettings.prd.json services/BasicInformation.API/appsettings.json
cp services/appsettings.prd.json services/CadastroEletronico.API/appsettings.json
cp services/appsettings.prd.json services/Client.API/appsettings.json
cp services/appsettings.prd.json services/Customer.API/appsettings.json
cp services/appsettings.prd.json services/CustomerExport.API/appsettings.json
cp services/appsettings.prd.json services/DataValidation.API/appsettings.json
cp services/appsettings.prd.json services/Service.Authentication.Identity/appsettings.json
cp services/appsettings.prd.json services/Service.Authorization.API/appsettings.json
cp services/appsettings.prd.json services/Workflow.API/appsettings.json
rm -rf services/Job.ConsoleApp/AppSettings.json
cp services/Job.ConsoleApp/AppSettings.prd.json services/Job.ConsoleApp/AppSettings.json
if [[ ! -e /var/cedro/configs ]]; then
    mkdir /var/cedro/configs
fi
cp services/appsettings.prd.json /var/cedro/configs/appsettings.json

#Processo de geração e disponibilização da da chave de segurança
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
rm -rf Security.Service.API/Release/netcoreapp1.0/server_key.xml
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.crp
cp defaultconnection.prd.txt Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rmi -f indusval/cryptokey || echo Creating cryptokey image
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rm -f cryptokey || echo Creating cryptokey image
docker build -t indusval/cryptokey ./Security.Service.API/Release/netcoreapp1.0 --no-cache --force-rm

docker-compose build --no-cache --force-rm

