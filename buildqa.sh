#!/bin/bash
rm -rf nginx
rm -rf .env
cp -R nginx_hml nginx
cp setup.container .env

rm -rf /var/cedro/configs/appsettings.json
cp services/appsettings.qa.json services/Backlog.API/appsettings.json
cp services/appsettings.qa.json services/BackLogPDL.API/appsettings.json
cp services/appsettings.qa.json services/BackOffice.API/appsettings.json
cp services/appsettings.qa.json services/BasicInformation.API/appsettings.json
cp services/appsettings.qa.json services/CadastroEletronico.API/appsettings.json
cp services/appsettings.qa.json services/Client.API/appsettings.json
cp services/appsettings.qa.json services/Customer.API/appsettings.json
cp services/appsettings.qa.json services/CustomerExport.API/appsettings.json
cp services/appsettings.qa.json services/DataValidation.API/appsettings.json
cp services/appsettings.qa.json services/Service.Authentication.Identity/appsettings.json
cp services/appsettings.qa.json services/Service.Authorization.API/appsettings.json
cp services/appsettings.qa.json services/Workflow.API/appsettings.json
rm -rf services/Job.ConsoleApp/AppSettings.json
cp services/Job.ConsoleApp/AppSettings.hml.json services/Job.ConsoleApp/AppSettings.json
if [[ ! -e /var/cedro/configs ]]; then
    mkdir /var/cedro/configs
fi
cp services/appsettings.qa.json /var/cedro/configs/appsettings.json

#Processo de geração e disponibilização da da chave de segurança
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
rm -rf Security.Service.API/Release/netcoreapp1.0/server_key.xml
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.crp
cp defaultconnection.hml.txt Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rmi -f indusval/cryptokey || echo Creating cryptokey image
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rm -f cryptokey || echo Creating cryptokey image
docker build -t indusval/cryptokey ./Security.Service.API/Release/netcoreapp1.0 --no-cache --force-rm

docker-compose build --no-cache --force-rm
