#!/bin/bash
rm -rf nginx
rm -rf .env
cp -R nginx_dev nginx
cp setup.container .env

rm -rf /var/cedro/configs/appsettings.json
cp services/appsettings.dev.json services/Backlog.API/appsettings.json
cp services/appsettings.dev.json services/BackLogPDL.API/appsettings.json
cp services/appsettings.dev.json services/BackOffice.API/appsettings.json
cp services/appsettings.dev.json services/BasicInformation.API/appsettings.json
cp services/appsettings.dev.json services/CadastroEletronico.API/appsettings.json
cp services/appsettings.dev.json services/Client.API/appsettings.json
cp services/appsettings.dev.json services/Customer.API/appsettings.json
cp services/appsettings.dev.json services/CustomerExport.API/appsettings.json
cp services/appsettings.dev.json services/DataValidation.API/appsettings.json
cp services/appsettings.dev.json services/Service.Authentication.Identity/appsettings.json
cp services/appsettings.dev.json services/Service.Authorization.API/appsettings.json
cp services/appsettings.dev.json services/Workflow.API/appsettings.json
rm -rf services/Job.ConsoleApp/AppSettings.json
cp services/Job.ConsoleApp/AppSettings.dev.json services/Job.ConsoleApp/AppSettings.json
if [[ ! -e /var/cedro/configs ]]; then
    mkdir /var/cedro/configs
fi
cp services/appsettings.dev.json /var/cedro/configs/appsettings.json

#Processo de geração e disponibilização da da chave de segurança
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
rm -rf Security.Service.API/Release/netcoreapp1.0/server_key.xml
rm -rf Security.Service.API/Release/netcoreapp1.0/defaultconnection.crp
cp defaultconnection.dev.txt Security.Service.API/Release/netcoreapp1.0/defaultconnection.txt
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rmi -f indusval/cryptokey || echo Creating cryptokey image
docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rm -f cryptokey || echo Creating cryptokey image
docker build -t indusval/cryptokey ./Security.Service.API/Release/netcoreapp1.0 --no-cache --force-rm

docker-compose build --no-cache --force-rm

if [[ ! -e /var/cedro/management ]]; then
    mkdir /var/cedro/management
fi
docker ps -a -q --filter "name=management" | grep -q . && docker rm -f management || echo Creating management container
docker run --name management -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v /var/cedro/management:/data portainer/portainer
cp resources/portainer.db /var/cedro/management/portainer.db