#!/bin/bash
if [[ $# -eq 0 ]] ; then
    printf '\nUsage: ./%s <"string de conexão entre aspas">      ...\n\n' "$(basename "$0")" >&2
    exit 64
fi

echo "-----------------------------------------------------------------------"
echo "                              Crypto Key                              -"
echo "                                                                      -"
echo "  Copiar a chave gerada no arquivo data_encrypt.txt para a prop       -"
echo "  DefaultConnection no appsettings.env.json referente ao seu ambiente -"
echo "                                                                      -"
echo "-----------------------------------------------------------------------"
echo ""
dotnet Security.Service.API/Release/netcoreapp1.0/Security.Service.API.dll key
sudo rm -rf server_key.xml
sudo cp Security.Service.API/Release/netcoreapp1.0/server_key.xml server_key.xml
dotnet Security.Service.API/Release/netcoreapp1.0/Security.Service.API.dll encrypt "$1"

