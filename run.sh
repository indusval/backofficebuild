#!/bin/bash
rm -rf /var/cedro/configs/defaultconnection.crp
rm -rf /var/cedro/keyring/server_key.xml

docker-compose up -d --force-recreate

docker ps -a -q --filter "name=cryptokey" | grep -q . && docker rm -f cryptokey || echo Removing existing cryptokey container
docker run --name cryptokey -d -ti indusval/cryptokey
docker exec -it cryptokey dotnet /app/Security.Service.API/Security.Service.API.dll key
docker exec -it cryptokey dotnet /app/Security.Service.API/Security.Service.API.dll encrypt defaultconnection.txt
docker cp cryptokey:/app/Security.Service.API/data_encrypt.txt /var/cedro/configs/connectionstring.crp
docker cp cryptokey:/app/Security.Service.API/server_key.xml /var/cedro/keyring/server_key.xml
